# Introduction to the SICdb

The SICdb (Salzburg intensive care database) is an ICU dataset from the SILK and PMU in Salzburg. This repository 
contains a introduction to the structure and data of this dataset. Also we try to provide further information to the 
connection to the MIMIC-IV dataset. Also the code belongs to this paper (DOI:)

# Data

We download the SICdb from [physionet](https://physionet.org/content/sicdb/1.0.6/) and put the csv files into the folder
[SICdb](./SICdb).For the vital signs (data_float_h) we extracted the binary data with the 
[code](https://github.com/nrodemund/sicdb/tree/main/Scripts/Unpack%20raw%20data/unpack.py) their
[github page](https://github.com/nrodemund/sicdb/) and [wiki](https://www.sicdb.com/Documentation) provide us.
Please be aware that the signal data is **around 60 GB big**.

For our computation we are using the csv file but the suggested database-docker solution also works good.

# Scripts

For the visualisation we are using the python jupyter-notebook library. After installing the jupyter notebook we start 
it in the terminal with 

`jupyter-notebook`

If you are not familiar with this package, please view this [site](https://jupyter.org/)

The scripts are split into:
- general information
- laboratory information
- vital preprocessing
- vital information